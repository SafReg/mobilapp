A cél egy android (7+) telefonos háztartásvezető és segítő applikáció. 
Regisztráció, a család háztartása amiben a felhasználó él, és a közös háztartásjelszó megadása után 
a felhasználó bekerülne az "online család"-jába attól függően hogy milyen szerepet tölt be a család
életében, lehet szülő (maximum 2 egy családon belül), vagy gyerek user.
A szülő userek oszthatnának ki egymásnak akár, vagy a gyerekeknek kötelező tennivalókat, 
vagy tehetnének ki opcionális feladatokat adott pontszámért, amit a család tagjai közül bárki el tud vállalni. 
Teljesítés és validáció után pedig megkapná érte a feladatért járó pontszámot, 
amelyekből eleget összegyűjtve szülő által megadott jutalomra válthatna. 
Minden user feltölthetné a saját egész heti órarendjét mely a család többi tagja előtt nyilvános, és amelyben szerepelnének a 
fix délutáni programok is. Ezt mindenki kiegészíthetné az adott hétre váró egyéb programokkal, 
így senki nem lepődne meg ha valakit nem találna otthon. 
A két szülő user tudna közös szavazásokat létrehozni amikben a család tagjai részt vehetnének, 
még akkor is ha nincsenek otthon. Lenne közös bevásárlólista is. Fel tudnák tölteni, hogy milyen alapanyagok,
ételek stb. vannak otthon. Fel lehetne vinni recepteket, és ezek után a program listázná azokat a recepteket, 
amelyeket az otthon lévő összetevőkből el lehet készíteni.