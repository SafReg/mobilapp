﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Calcul : MonoBehaviour
{
    public TMP_InputField num1Input;
    public TMP_InputField num2Input;
	public TextMeshProUGUI resultText;
	
	public void Multiply()
	{
		float num1 = float.Parse(num1Input.text);
		float num2 = float.Parse(num2Input.text);
		
		resultText.text = (num1*num2).ToString("F2");
	}
}
